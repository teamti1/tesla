const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/app.js')
   .scripts([
       'resources/js/fontawesome.js',
       'resources/js/sb-admin.min.js'
   ], 'public/js/dashboard.js')
   .css('resources/sass/app.scss','public/css/app.css')
   .styles([
       'resources/css/sb-admin.css',
       'resources/css/fontawesome.css',
   ], 'public/css/dashboard.css');
