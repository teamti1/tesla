@extends('layouts.dashboard')

@section('content')
            <div class="card">
                <div class="card-header">Listado de usuarios</div>

                <div class="card-body"> 
                    <div class="table-responsive">   
                        <table class="table table-bordered">                    
                            <thead>
                                <tr class="text-white bg-primary">               
                                    <th>Nombre</th>
                                    <th>Usuario</th>
                                    <th>Fecha de registro</th>
                                </tr>
                            </thead>
                            <tbody> 
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>    
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->created_at}}</td> 
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>  
@endsection
