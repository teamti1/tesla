<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->dateTime('born_date');
            $table->dateTime('born_start');
            $table->char('gender');
            $table->string('cuip');
            $table->string('curp');
            $table->string('rfc');
            $table->integer('number_employee');
            $table->integer('tel');
            $table->string('civil_status');
            $table->string('scholarship');
            $table->string('picture');
            $table->string('fingerprints');
            $table->string('military_primer');
            $table->string('institution');
            $table->string('confidence_control_exam');
            $table->integer('employment_id')->unsigned();
            $table->timestamps();
            $table->foreign('employment_id')->references('id')->on('employments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elements');
    }
}
