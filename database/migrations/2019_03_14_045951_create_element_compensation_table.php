<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementCompensationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_compensation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('elements_id')->unsigned();
            $table->integer('compensation_id')->unsigned();
            $table->timestamps();
            $table->foreign('compensation_id')->references('id')->on('compensations');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_compensation');
    }
}
